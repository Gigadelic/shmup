﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBoundryController : MonoBehaviour {
    
    private void OnTriggerEnter(Collider other)
    {
        //If the player's bullet hit the boundry
        if(other.gameObject.tag == "PlayerBullet" || other.gameObject.tag == "EnemyBullet")
        {
            other.gameObject.GetComponent<BulletController>().BulletHitWall();            
        }
    }
}
