﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInitializerController : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        EventManager.Instance.DispatchGameEvent(GameEventEnum.LEVEL_LOADED, new GameEventArgs(player));
        Debug.Log("THIS GOT HIT");
    }
}
