﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionController : MonoBehaviour
{
    public Animator anim;

    public void CloseDoorTransition()
    {
        anim.SetBool("IsDoorOpen",false);
    }   

    public void OpenDoorTransition()
    {
        anim.SetBool("IsDoorOpen", true);
    }
}
