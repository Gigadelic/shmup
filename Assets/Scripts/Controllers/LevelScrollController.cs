﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScrollController : MonoBehaviour {

    public float scrollSpeed;
    public float wallSetSpawnOffset;
    public float wallSetSpawnSpeed;
    
    public GameObject background;

    public List<GameObject> listOfWallSets;
    public Queue<GameObject> queueOfWallSets;

    public float lastSpawnPosition;
    private float nextWallSetSpawn;

	// Use this for initialization
	void Start () {
        queueOfWallSets = new Queue<GameObject>();
        //Get Last one of the list at the start ** must be ordered from left to right in inspector
        foreach (var wall in listOfWallSets)
        {
            queueOfWallSets.Enqueue(wall);
            //lastSpawnPosition = wall.transform.position.x;
        }        
    }

    // Update is called once per frame
    void Update()
    {
        background.transform.Translate(0f, Time.deltaTime * scrollSpeed, 0f);

        //queueOfWallSets.Peek().transform.position
        if (Time.time > nextWallSetSpawn)
        {
            nextWallSetSpawn = Time.time + wallSetSpawnSpeed;
            GameObject wall = queueOfWallSets.Dequeue();
            wall.transform.localPosition = new Vector3(0f, lastSpawnPosition + wallSetSpawnOffset, 0f);
            queueOfWallSets.Enqueue(wall);           
            lastSpawnPosition += wallSetSpawnOffset;
        }
    }
}
