﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShotSpawnType
{
    FourSpinConfig,
    EightSpinConfig,
    TwoSeparateSpinConfig,
    SingleFrontConfig,
    ThreeFrontConfig,
    SingleFrontTowardsPlayer,
    ThreeFrontTowardsPlayer
}

[System.Serializable]
public class ShotSpawnConfig
{
    public ShotSpawnType shotSpawnType;
    public GameObject shotSpawnConfig;
    public bool isSelected;
}


public class SpawnShotController : MonoBehaviour {
    
    public GameObject enemyBullet;

    public List<ShotSpawnConfig> listOfShotSpawnConfig;

    public bool canFire = false;    
    public float fireRate;
    public float spinSpeed;
    public bool reverseSpin;
    public float homingoffset = 180f;
    public Transform shotStorage;

    float nextShot;
    
    Transform homingTarget; 

    public void SetHomingTarget(Transform homingTarget)
    {
        this.homingTarget = homingTarget;
    }

    public void SetShotPattern(ShotSpawnType shotSpawnType)
    {
        canFire = false;
        listOfShotSpawnConfig.ForEach(t => t.isSelected = false);
       
        foreach (ShotSpawnConfig config in listOfShotSpawnConfig)
        {
            if(config.shotSpawnType == shotSpawnType)
            {
                config.isSelected = true;
                break;
            }                
        }
        canFire = true;
    }

    public void SetShotPattern(List<ShotSpawnType> listOfShowSpawnTypes)
    {
        canFire = false;
        foreach (ShotSpawnType type in listOfShowSpawnTypes)
        {
            foreach (ShotSpawnConfig config in listOfShotSpawnConfig)
            {
                if (config.shotSpawnType == type)
                {
                    config.isSelected = true;
                    break;
                }
            }
        }
        canFire = true;
    }

    void Update()
    {
        if(canFire)
        {        
            if (Time.time > nextShot)
            {
                nextShot = Time.time + fireRate;
                foreach(ShotSpawnConfig config in listOfShotSpawnConfig)
                {
                    if (config.isSelected)
                    {
                        if(config.shotSpawnType == ShotSpawnType.SingleFrontConfig || config.shotSpawnType == ShotSpawnType.ThreeFrontConfig)
                        {
                            for (int i = 0; i < config.shotSpawnConfig.transform.childCount; i++)
                            {
                                GameObject bullet = Instantiate(enemyBullet, config.shotSpawnConfig.transform.GetChild(i).transform.position, config.shotSpawnConfig.transform.GetChild(i).rotation);
                                bullet.transform.SetParent(shotStorage.transform);
                            }
                        }

                        if (config.shotSpawnType == ShotSpawnType.SingleFrontTowardsPlayer || config.shotSpawnType == ShotSpawnType.ThreeFrontTowardsPlayer)
                        {
                            for (int i = 0; i < config.shotSpawnConfig.transform.childCount; i++)
                            {
                                GameObject bullet = Instantiate(enemyBullet, config.shotSpawnConfig.transform.GetChild(i).transform.position, config.shotSpawnConfig.transform.GetChild(i).rotation);
                                bullet.transform.SetParent(shotStorage.transform);
                            }
                            SpinShotConfigTowardsPlayer(config.shotSpawnConfig.transform);
                        }

                            if (config.shotSpawnType == ShotSpawnType.FourSpinConfig || config.shotSpawnType == ShotSpawnType.EightSpinConfig)
                        {                        
                            for (int i = 0; i < config.shotSpawnConfig.transform.childCount; i++)
                            {
                                GameObject bullet = Instantiate(enemyBullet, config.shotSpawnConfig.transform.GetChild(i).transform.position, config.shotSpawnConfig.transform.GetChild(i).rotation);
                                bullet.transform.SetParent(shotStorage.transform);                         
                            }
                            SpinShotConfig(config.shotSpawnConfig.transform);
                        }
                        else if(config.shotSpawnType == ShotSpawnType.TwoSeparateSpinConfig)
                        {
                            //This is the logic path for 2 SeparateSpins (flower Pattern)
                            for (int i = 0; i < config.shotSpawnConfig.transform.GetChild(0).childCount; i++)
                            {
                                GameObject bullet = Instantiate(enemyBullet, config.shotSpawnConfig.transform.GetChild(0).GetChild(i).transform.position, config.shotSpawnConfig.transform.GetChild(0).GetChild(i).rotation);
                                bullet.transform.SetParent(shotStorage.transform);
                            }
                            SpinShotConfig(config.shotSpawnConfig.transform.GetChild(0));

                            for (int i = 0; i < config.shotSpawnConfig.transform.GetChild(1).childCount; i++)
                            {
                                GameObject bullet = Instantiate(enemyBullet, config.shotSpawnConfig.transform.GetChild(1).GetChild(i).transform.position, config.shotSpawnConfig.transform.GetChild(1).GetChild(i).rotation);
                                bullet.transform.SetParent(shotStorage.transform);
                            }
                            SpinShotConfig(config.shotSpawnConfig.transform.GetChild(1),true);
                        }
                    }
                }                                                
            }
        }
    }

    void SpinShotConfig(Transform configTransform, bool saparateSpin = false)
    {
        if(saparateSpin)
        {
            if (!reverseSpin)
                configTransform.Rotate(configTransform.up, spinSpeed * Time.deltaTime);
            else
                configTransform.Rotate(configTransform.up, spinSpeed * Time.deltaTime * -1);
        }
        else
        {
            if (!reverseSpin)
                configTransform.Rotate(configTransform.up, spinSpeed * Time.deltaTime*-1);
            else
                configTransform.Rotate(configTransform.up, spinSpeed * Time.deltaTime);
        }
        
    }
    
    void SpinShotConfigTowardsPlayer(Transform configTransform)
    {
    
        Vector3 difference = homingTarget.position - configTransform.position;
        difference.Normalize();
        float rotation_y = Mathf.Atan2(difference.x, difference.z) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, rotation_y + homingoffset, 0f);
    }


}
