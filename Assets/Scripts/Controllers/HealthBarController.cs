﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarController : MonoBehaviour
{
    public GameObject healthBar;
    public Transform healthBarOffScreenPos;
    public Transform healthBarOnScreenPos;
    
    public Image healthBarProgress;
    public List<HealthBarStatusColor> healthBarStatusColors;

    private float maxHP;           

    public void SetUpHealthBar(float MaxHp)
    {
        maxHP = MaxHp;
        healthBarProgress.fillAmount = 1f;
        healthBarProgress.enabled = true;
        UpdateHealthBar(maxHP);
    }

    public void MakeHealthBarAppear()
    {
        iTween.MoveTo(healthBar, healthBarOnScreenPos.position, 1f);
    }

    public void MakeHealthBarDisappear()
    {
        iTween.MoveTo(healthBar, healthBarOffScreenPos.position, 1f);
    }

    public void UpdateHealthBar(float currentHp)
    {
        float percentOfHPLeft = (currentHp / maxHP) * 100f;
        healthBarProgress.fillAmount = percentOfHPLeft * 0.01f;

        foreach (HealthBarStatusColor threshold in healthBarStatusColors)
        {
            if (percentOfHPLeft > threshold.percentageThresholds.x && percentOfHPLeft <= threshold.percentageThresholds.y)
                healthBarProgress.color = threshold.color;
        }

        if (currentHp == 0)
            healthBarProgress.enabled = false;

    }
    
}

[System.Serializable]
public class HealthBarStatusColor
{
    [Header("X is Min Y is Max")]
    public Vector2 percentageThresholds; // X is Min | Y is Max
    public Color color;
}

