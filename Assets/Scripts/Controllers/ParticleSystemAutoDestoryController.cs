﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemAutoDestoryController : MonoBehaviour
{
    public ParticleSystem ps;    

    public void Update()
    {
        if (ps)
        {
            if (!ps.IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }
}
