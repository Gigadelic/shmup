﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public enum MessageType
{
    Default,
    MissionStart,
    MissionComplete,
    GameOver
}


public class MessageController : MonoBehaviour
{
    //public string messageTextToDisplay;
    public int introDelayTime;
    public Sprite goodMessageBackground;
    public Sprite badMessageBackground;
    public Sprite informationalMessageBackground;
    public TextMeshProUGUI LeftText;
    public TextMeshProUGUI RightText;
    public Image backgroundImage;
    public Animator anim;

    private float messageDelay;
    private bool canDisplayMessage = false;
    private MessageType messageType;
    public void DisplayMessage(string MessageText, MessageType messageType=0, float delay=0)
    {
        switch (messageType)
        {
            case MessageType.MissionStart:
            case MessageType.MissionComplete:
                backgroundImage.sprite = goodMessageBackground;
                break;
            case MessageType.GameOver:
                backgroundImage.sprite = badMessageBackground;
                break;
            case MessageType.Default:
                backgroundImage.sprite = informationalMessageBackground;
                break;
        }   

        LeftText.text = MessageText;
        RightText.text = MessageText;
        messageDelay = Time.time + delay;
        canDisplayMessage = true;
        this.messageType = messageType;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > messageDelay && canDisplayMessage)
        {
            canDisplayMessage = false;
            anim.SetTrigger("StartTextFlyIn");
        }
    }

    void MessageDisplayComplete()
    {
        EventManager.Instance.DispatchGameEvent(GameEventEnum.MESSAGE_DISPLAY_COMPLETE, new GameEventArgs((int)messageType));
    }
}
