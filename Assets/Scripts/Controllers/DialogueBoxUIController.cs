﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueBoxUIController : MonoBehaviour
{
    public TextMeshProUGUI txtNPCName;
    public TextMeshProUGUI txtDialogueArea;
    public Animator anim;


    public void SetNPCName(string npcName)
    {
        txtNPCName.text = "";
        txtDialogueArea.text = "";

        txtNPCName.text = npcName;
        anim.SetBool("IsOpen", true);
    }

    public void DisplayDialog(string dialogue)
    {
        txtDialogueArea.text = "";
        StopAllCoroutines();
        StartCoroutine(WriteText(dialogue));
    }    

    IEnumerator WriteText(string sentence)
    {
        char[] dialogueCharacters = sentence.ToCharArray();

        for (int i = 0; i < dialogueCharacters.Length; i++)
        {
            txtDialogueArea.text += dialogueCharacters[i];
            yield return null;
        }        
    }

    public void CloseDialogueBox()
    {
        anim.SetBool("IsOpen", false);
        
    }

}
