﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstBossController : ShipController
{
    public Animator anim;
    public bool isShootingFromGuns = false;
    private float shotNextUse;
    public float amountOfTimeInIdle = 10f;
    public float amountOfTimeForlaserCoolDown = 5f;
    public float amountOfTimeToMoveAround = 10f;
    public HitBoxController hitBox;
    public GameObject damageParticles;
    public GameObject laserChargeParticle;
    public GameObject laserEnergyParticle;
    public GameObject laserCoolDownSmoke;
    public List<SpawnShotController> listOfSpawnShotController;


    private float nextIdleCycle;
    private float nextlaserCoolDown;
    private float nextMoveCycle;

    private bool isMoving = false;
    private bool isInLaserMode = false;    
    private bool isLaserOnCoolDown = false;

    private bool isAlive = true;
    private enum BossState { Idle, Moving, LaserAttack,LaserCooldown,Dead}
    private BossState currentState;

    private int moveCycleCount = 0;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        nextIdleCycle = Time.time + amountOfTimeInIdle;
        if(!hitBox)
            hitBox = GetComponentInChildren<HitBoxController>();
        hitBox.DisableCollider();

        //Deactivate All Particles
        damageParticles.SetActive(false);
        laserChargeParticle.SetActive(false);
        laserEnergyParticle.SetActive(false);
        laserCoolDownSmoke.SetActive(false);
    }

    
    public override void TakeDamage(float dmgAmount)
    {
        base.TakeDamage(dmgAmount);
        EventManager.Instance.DispatchGameEvent(GameEventEnum.BOSS_TOOK_DAMAGE, new GameEventArgs(HP));
    }

    public override void ShipHasBeenDestoryed()
    {
        isAlive = false;
        damageParticles.SetActive(true);
        anim.SetBool("IsBossMovingAround", false);
        anim.SetTrigger("hasBeenDefeated");        
        StartCoroutine(BossIsDefeated());
    }

    IEnumerator BossIsDefeated()
    {
        EventManager.Instance.DispatchGameEvent(GameEventEnum.BOSS_HAS_BEEN_DEFEATED, GameEventArgs.Empty);
        yield return new WaitForSeconds(5f);
        Instantiate(explosionParticle, hitBox.transform.position, Quaternion.identity);
        Destroy(gameObject);
        EventManager.Instance.DispatchGameEvent(GameEventEnum.BOSS_HAS_EXPLODED, GameEventArgs.Empty);
        listOfSpawnShotController.ForEach(t => t.canFire = false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            #region Boss behavior outside the Laser State

            //How long has the boss been in Idle?
            if (Time.time > nextIdleCycle && !isMoving && !isInLaserMode)
            {
                Debug.Log("Is Moving");
                //Make Boss Move around
                anim.SetBool("IsBossMovingAround", true);
                nextMoveCycle = Time.time + amountOfTimeToMoveAround;
                isMoving = true;
                listOfSpawnShotController.ForEach(t => t.SetShotPattern(ShotSpawnType.ThreeFrontConfig));
            }

            //Is it time to stop Moving?
            if (Time.time > nextMoveCycle && isMoving && !isInLaserMode)
            {
                Debug.Log("Stop Moving");
                anim.SetBool("IsBossMovingAround", false);
                nextIdleCycle = Time.time + amountOfTimeInIdle;
                isMoving = false;
                moveCycleCount++;
                listOfSpawnShotController.ForEach(t => t.SetShotPattern(ShotSpawnType.FourSpinConfig));
            }

            #endregion

            if (moveCycleCount >= 2)
            {
                moveCycleCount = 0;
                Debug.Log("laser Time!");
                anim.SetBool("IsLazerOpen", true);
                isShootingFromGuns = false;
                isInLaserMode = true;
                listOfSpawnShotController.ForEach(t => t.canFire = false);
            }

            //Is it time to close the laser door?
            if (Time.time > nextlaserCoolDown && isLaserOnCoolDown)
            {
                anim.SetBool("IsLazerOpen", false);
                isInLaserMode = false;
                isLaserOnCoolDown = false;
                isShootingFromGuns = true;
                listOfSpawnShotController.ForEach(t => t.canFire = false);
            }
        }
    }

    void BossHasAppeared()
    {
        Debug.Log("Boss Has Appeared");
        isShootingFromGuns = true;
        listOfSpawnShotController.ForEach(t => t.canFire = true);
        EventManager.Instance.DispatchGameEvent(GameEventEnum.BOSS_HAS_APPEARED, new GameEventArgs(HP));
    }

    void BossLaserIsOpen()
    {        
        //Turn On Hit Box
         Debug.Log("Laser IS OPEN");
        hitBox.EnableCollider();
        anim.SetTrigger("IsLaserMode");
        laserChargeParticle.SetActive(true);
    }

    void BossLaserIsClosed()
    {
        //Turn Off Hit Box
        hitBox.DisableCollider();
        Debug.Log("Laser IS CLOSED");
        laserCoolDownSmoke.SetActive(false);
    }

    void FireLaser()
    {
        hitBox.GetComponent<LaserController>().ActivateLaser();
        laserChargeParticle.SetActive(false);
        laserEnergyParticle.SetActive(true);
    }

    void StopLaser()
    {
        hitBox.GetComponent<LaserController>().DisableLaser();
        nextlaserCoolDown = Time.time + amountOfTimeForlaserCoolDown;
        isLaserOnCoolDown = true;
        laserEnergyParticle.SetActive(false);
        laserCoolDownSmoke.SetActive(true);
    }
}
