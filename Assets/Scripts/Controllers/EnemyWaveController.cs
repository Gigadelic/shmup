﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaveController : MonoBehaviour
{

    public List<ShipController> listOfEnemiesInWave;    
    
    public void EnemyHasBeenDestoryed(ShipController enemyShip)
    {
        if (listOfEnemiesInWave.Contains(enemyShip))
            listOfEnemiesInWave.Remove(enemyShip);

        if(listOfEnemiesInWave.Count <= 0)
        {
            //wave is over spawn next wave
            EventManager.Instance.DispatchGameEvent(GameEventEnum.ENEMY_WAVE_DESTROYED, new GameEventArgs(this));
        }
    }

    private void OnEnable()
    {
        listOfEnemiesInWave.ForEach(t => t.gameObject.SetActive(true));
    }
}
