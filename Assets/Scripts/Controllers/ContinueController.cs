﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueController : MonoBehaviour
{
    public int amountOfContinuesLeft = 3;
    bool canPressButton = false;
    public PlayerController player;
    public Transform playerSpawnPoint;
    public Image OneUpImage;

    private Queue<Image> queueOfOneUpsOnScreen = new Queue<Image>();
    
    public void SetUpPlayerContinues()
    {
        for (int i = 0; i < amountOfContinuesLeft; i++)
        {
            Image OneUpIcon = Instantiate(OneUpImage);
            OneUpIcon.transform.SetParent(transform, false);
            queueOfOneUpsOnScreen.Enqueue(OneUpIcon);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(canPressButton)
        {
            if (Input.GetButtonUp("Jump"))
            {
                Debug.Log("Respawn Player");
                canPressButton = false;
                PlayerController newPlayer = Instantiate(player, playerSpawnPoint.position, Quaternion.identity);
                EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_HAS_RESPAWNED, new GameEventArgs(newPlayer));

                //Remove a Continue From the player
                amountOfContinuesLeft--;
                Destroy(queueOfOneUpsOnScreen.Dequeue().gameObject);
            }
        }
    }

    public void PlayerHasDied()
    {
        //Is it Game Over ?
        if(amountOfContinuesLeft <= 0)
        {
            //Game Over
            EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_IS_GAME_OVER, GameEventArgs.Empty);
        }
        else
        {
            //Prompt Player to press a button so that they can continue.
            canPressButton = true;                
        }
    }

    public void ResetContinues()
    {
        queueOfOneUpsOnScreen.Clear();
        amountOfContinuesLeft = 3;
        canPressButton = false;

        foreach(Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

}
