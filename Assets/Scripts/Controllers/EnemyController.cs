﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : ShipController
{    
    public string playerBulletTag;    
    public float shipSpeed = 2f;
    public bool canShoot = false;
    public DropTableController dropTable;
    public float MaxDistance = -30f;

    private float shotNextUse;

    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        //Move Ship
        transform.Translate(new Vector3(0f,0f, -1 * shipSpeed* Time.deltaTime));

        //Fire
        if (canShoot)
        {
            if (Time.time > shotNextUse)
            {
                shotNextUse = Time.time + ShotSpeed;
                foreach (Transform shotSpawn in listOfShotSpawn)
                {
                    GameObject newBullet = Instantiate(bulletPattern, shotSpawn.position, Quaternion.identity) as GameObject;
                    newBullet.transform.SetParent(shotContainer);
                }
            }
        }

        if(transform.position.z <= MaxDistance)
        {
            Destroy(gameObject);
            EventManager.Instance.DispatchGameEvent(GameEventEnum.ENEMY_SHIP_DESTROYED, new GameEventArgs(this));
        }
            
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == playerBulletTag)
        {
            //Take Damage from the players bullet;
            TakeDamage(other.gameObject.GetComponent<BulletController>().dmgAmount);
            Destroy(other.gameObject);
        }
    }    

    public override void ShipHasBeenDestoryed()
    {
        base.ShipHasBeenDestoryed();
        PowerUpType drop = dropTable.PickItemFromTable();
        if(drop != PowerUpType.None)        
            Instantiate(dropTable.GetDropByType(drop), transform.position, Quaternion.identity);

        Debug.Log("Dropped: " + drop);

        EventManager.Instance.DispatchGameEvent(GameEventEnum.ENEMY_SHIP_DESTROYED, new GameEventArgs(this));
    }
}
