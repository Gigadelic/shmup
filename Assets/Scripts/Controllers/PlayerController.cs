﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : ShipController {

    
    public float flightSpeed;
    public GameObject shipModelContainer;
    public CharacterController controller;
    public Animator anim;

    public float maxShipTilt = 15f;
    public float turnSmoothTime = 0.1f;

    public int currentShotLvl = 1;
    public List<GameObject> listOfShotPatterns;
    public GameObject pickUpParticle;
    public ShieldController shieldController;
    public GameObject shipGraphics;
    public GameObject laserGun;
    public List<LaserController> listOfLaserController;


    float turnSmoothVelocity;
    private float shotNextUse;
    HitBoxController hitBox;
    bool isDoingABarrelRoll;
    bool canShoot = true;
    bool isShieldOn = false;
    bool isLaserOn = false;
    public override void Start()
    {
        base.Start();
        hitBox = GetComponentInChildren<HitBoxController>();        
    }

    public override void TakeDamage(float dmgAmount)
    {
        if(!isDoingABarrelRoll)
            base.TakeDamage(dmgAmount);
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        float inputMagnitude = Mathf.Abs(horizontal) + Mathf.Abs(vertical);        

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        //If player is moving
        if(inputMagnitude >= 0.1f)
        {            
            controller.Move(direction * flightSpeed * Time.deltaTime);
        }
        
        //Tilt ship when going side to side.
        if (!isDoingABarrelRoll)
        {
            float angle = Mathf.SmoothDampAngle(shipGraphics.transform.eulerAngles.x, maxShipTilt* horizontal, ref turnSmoothVelocity, turnSmoothTime);
            shipGraphics.transform.rotation = Quaternion.Euler(angle, 90f, 0f);
        }        

        //Shoot Logic       
        if (Time.time > shotNextUse)
        {            
            if (Input.GetAxis("Fire1") > 0 && canShoot)
            {
                shotNextUse = Time.time + ShotSpeed;
                foreach (Transform shotSpawn in listOfShotSpawn)
                {
                    GameObject newBullet = Instantiate(bulletPattern, shotSpawn.position, Quaternion.identity) as GameObject;
                    newBullet.transform.SetParent(shotContainer);
                }                
                EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_HAS_SHOT,GameEventArgs.Empty);
            }
        }

        //Fire Laser Logic
        if (Input.GetAxis("Fire1") > 0 && canShoot && isLaserOn)
        {
            listOfLaserController.ForEach(t => t.ActivateLaser());
        }
        else
        {
            listOfLaserController.ForEach(t => t.DisableLaser());
        }

        //Barrel Roll Logic
        if(Input.GetKeyDown(KeyCode.C) && isDoingABarrelRoll == false)
        {
            isDoingABarrelRoll = true;
            canShoot = false;
            hitBox.DisableCollider();
            iTween.RotateBy(shipGraphics, iTween.Hash(
                "amount", new Vector3(1f, 0f, 0f),
                "time",0.5f,
                "easetype","easeOutQuint",
                "oncomplete", "BarrelRollComplete",
                "oncompletetarget", this.gameObject
            )); ;
            EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_IS_DOING_A_BARREL_ROLL, GameEventArgs.Empty);
        }
        
    }    

    public override void ShipHasBeenDestoryed()
    {
        base.ShipHasBeenDestoryed();
        EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_HAS_DIED, GameEventArgs.Empty);
    }

    public void Respawn()
    {
        anim.Play("PlayerRespawn");
        if(hitBox == null)
            hitBox = GetComponentInChildren<HitBoxController>();
        hitBox.DisableCollider();        
    }

    void RespawnComplete()
    {
        hitBox.EnableCollider();
    }

    public void PlayerPickedUpPowerUp(PowerUpType type)
    {
        GameObject newPickUpParticle = Instantiate(pickUpParticle, transform.position, Quaternion.identity);
        newPickUpParticle.transform.SetParent(transform);
        switch (type)
        {
            case PowerUpType.ShootLvlUp:
                currentShotLvl++;
                if (currentShotLvl > listOfShotPatterns.Count)
                    currentShotLvl = listOfShotPatterns.Count;
                bulletPattern = listOfShotPatterns[currentShotLvl - 1];
                break;
            case PowerUpType.Shield:
                EnableShield();

                break;
            case PowerUpType.SpecialAtk:
                isLaserOn = true;
                laserGun.SetActive(true);
                listOfLaserController.ForEach(t => t.DisableLaser());
                break;
        }
    }  
    
    public void DisableShield()
    {
        shieldController.gameObject.SetActive(false);
        hitBox.EnableCollider();
        isShieldOn = false;
    }

    public void EnableShield()
    {
        shieldController.gameObject.SetActive(true);
        hitBox.DisableCollider();
        isShieldOn = true;
    }

    //Called by Itween
    void BarrelRollComplete()
    {
        if(!isShieldOn)
            hitBox.EnableCollider();

        isDoingABarrelRoll = false;
        canShoot = true;
    }
    
}

