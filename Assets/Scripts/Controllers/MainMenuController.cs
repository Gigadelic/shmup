﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MainMenuController : MonoBehaviour
{
    public List<Button> listOfButtons;
    
    private void Start()
    {
        EventManager.Instance.DispatchGameEvent(GameEventEnum.GAME_LOADED, GameEventArgs.Empty);
    }

    public void PlayerSelectedOption(int option)
    {
        listOfButtons.ForEach(t => t.interactable = false);        
        switch ((GameMenu)option)
        {
            case GameMenu.ShipSelect:
                EventManager.Instance.DispatchGameEvent(GameEventEnum.LOAD_SHIP_SELECT_MENU, GameEventArgs.Empty);
                break;
            case GameMenu.Level:
                EventManager.Instance.DispatchGameEvent(GameEventEnum.LOAD_LEVEL, new GameEventArgs(1)); //Load First Level
                break;
            case GameMenu.QuitGame:
                Application.Quit();
                break;
        }

    }
}

public enum GameMenu
{
    MainMenu,
    Options,
    ShipSelect,
    Level,
    QuitGame
}

