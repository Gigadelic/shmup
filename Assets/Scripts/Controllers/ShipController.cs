﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public float HP = 2;
    public GameObject explosionParticle;
    public GameObject bulletPattern;
    public List<Transform> listOfShotSpawn;
    public float ShotSpeed;
    public Transform shotContainer;

    virtual public void Start()
    {
        shotContainer = GameObject.FindGameObjectWithTag("BulletContainer").transform;
    }

    virtual public void TakeDamage(float dmgAmount)
    {
        if (dmgAmount * -1 + HP <= 0)
        {
            ShipHasBeenDestoryed();
        }
        else
        {
            HP += dmgAmount * -1;            
        }
    }

    virtual public void ShipHasBeenDestoryed()
    {        
        Instantiate(explosionParticle, transform.position, Quaternion.identity);        
        Destroy(gameObject);
    }
}
