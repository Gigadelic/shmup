﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorController : MonoBehaviour
{
    public float rotSpeed;    

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Time.deltaTime * rotSpeed, Time.deltaTime * rotSpeed, Time.deltaTime * rotSpeed);
    }
}
