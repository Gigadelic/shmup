﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour
{
    public LineRenderer lr;
    public string tagToLookFor;
    public float laserDmgIntervals = 0.05f;
    public float laserDmg = 1f;
    public GameObject laserBurnParticle;
    public List<GameObject> listOfVFXParticles;
    
    private bool lasersActivated = false;

    private float nextLaserShot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (lasersActivated)        
        {
            lr.enabled = true;
            lr.SetPosition(0, transform.position);
            RaycastHit hit;
           
            Debug.DrawRay(transform.position, transform.forward, Color.red);
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {

                if (hit.collider)
                {
                    if (hit.collider.gameObject.tag == tagToLookFor)
                    {
                        lr.SetPosition(1, hit.point);
                        if (Time.time > nextLaserShot)
                        {
                            nextLaserShot = Time.time + laserDmgIntervals;
                            hit.collider.gameObject.GetComponent<ShipController>().TakeDamage(laserDmg);
                        }
                    }
                    else
                        lr.SetPosition(1, new Vector3(transform.position.x, -20f, transform.forward.z * 100));
                }
            }
            else
                lr.SetPosition(1, transform.forward * 500);

            laserBurnParticle.transform.position = lr.GetPosition(1);
        }
        else
            lr.enabled = false;
    }
    public void ActivateLaser()
    {
        lasersActivated = true;
        listOfVFXParticles.ForEach(t => t.SetActive(true));
        laserBurnParticle.SetActive(true);
    }

    public void DisableLaser()
    {
        lasersActivated = false;
        listOfVFXParticles.ForEach(t => t.SetActive(false));
        laserBurnParticle.SetActive(false);
    }
}


