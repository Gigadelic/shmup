﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public bool isEnemyBullet;
    public float bulletSpeed;
    public Rigidbody rbd;
    public float dmgAmount = 1;
    
    // Update is called once per frame
    void Update()
    {      
        if(!isEnemyBullet)
            transform.Translate(Vector3.up * bulletSpeed * Time.deltaTime);
        else
            transform.Translate(Vector3.down * bulletSpeed * Time.deltaTime);
    }

    public void BulletHitWall()
    {        
        Destroy(transform.parent.gameObject, 0.5f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //If bullet hit wall or doors.
        if (collision.gameObject.layer == 10 || collision.gameObject.layer == 12)
            BulletHitWall();
    }
}
