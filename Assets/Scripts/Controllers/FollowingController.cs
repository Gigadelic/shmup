﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingController : MonoBehaviour
{

    public Transform target;
    public float speedToFollow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speedToFollow);
    }
}
