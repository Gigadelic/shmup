﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSelectController : MonoBehaviour
{
    public GameObject selecterArrow;
    public GameObject shipToRotate;
    public float rotationSpeed = 4f;    
    public Sprite mouseOverSprite;
    public Sprite shipSelectedSprite;

    private bool isRotating = false;
    private bool isShipSelected = false;
    public float initSpriteYpos;
    
    // Start is called before the first frame update
    void Start()
    {
        initSpriteYpos = selecterArrow.transform.localPosition.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(isRotating && !isShipSelected)
        {
            shipToRotate.transform.Rotate(0f, rotationSpeed * Time.deltaTime, 0f);

        }
    }

    private void OnMouseEnter()
    {
        if(!isShipSelected)
        {
            isRotating = true;
            selecterArrow.SetActive(true);
            EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_HOVERED_OVER_OPTION, GameEventArgs.Empty);
        }
        
    }

    private void OnMouseExit()
    {   
        if(!isShipSelected)
        {
            isRotating = false;
            shipToRotate.transform.rotation = new Quaternion();
            selecterArrow.SetActive(false);
        }        
    }

    private void OnMouseUpAsButton()
    {
        selecterArrow.GetComponent<SpriteRenderer>().sprite = shipSelectedSprite;
        iTween.PunchPosition(selecterArrow, new Vector3(0,5f), 1f);        
        isShipSelected = true;
        EventManager.Instance.DispatchGameEvent(GameEventEnum.LOAD_LEVEL, new GameEventArgs(1));
        EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_SELECTED_OPTION, GameEventArgs.Empty);
    }


}
