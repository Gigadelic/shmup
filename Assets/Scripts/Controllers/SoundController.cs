﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public GameSFX gameSFX;
    public GameBGM gameBGM;

    public float amountOfTimeForFadeOut = 1f;

    private AudioSource currentlyPlayingBGM;
    private AudioSource previouslyPlayingBGM;

    #region Game SFX
    public void PlayExplosionClip()
    {
        gameSFX.shipExplosion.Play();
    }

    public void PlayPlayerShot()
    {
        gameSFX.playerShot.Play();
    }

    public void PlayerBarrelRoll()
    {
        gameSFX.barrelRoll.Play();
    }

    public void PlayShieldBroken()
    {
        gameSFX.shieldBroken.Play();
    }

    public void PlayMouseHover()
    {
        gameSFX.mouseHoverOver.Play();
    }

    public void PlayMouseClick()
    {
        gameSFX.mouseClick.Play();
    }
    #endregion

    #region Game BGM
    public void PlayMainMenuMusic()
    {        
        gameBGM.mainMenu.Play();
        currentlyPlayingBGM = gameBGM.mainMenu;
    }

    public void PlayShipSelectMusic()
    {        
        gameBGM.shipSelectMenu.Play();        
    }

   public void PlayLevelMusic(int lvl)
    {
        
        switch (lvl)
        {
            case 1:
                gameBGM.level1.Play();
                currentlyPlayingBGM = gameBGM.level1;
                break;
        }

    }

    public void FadeOutBGM()
    {
        previouslyPlayingBGM = currentlyPlayingBGM;
        StopAllCoroutines();
        StartCoroutine(FadeBGM(previouslyPlayingBGM, amountOfTimeForFadeOut, previouslyPlayingBGM.volume, 0));
    }

    public void FadeInBGM(string sceneName)
    {
        previouslyPlayingBGM = currentlyPlayingBGM;
        StopAllCoroutines();
        switch (sceneName)
        {
            case "MainMenu":
                PlayMainMenuMusic();
                StartCoroutine(FadeBGM(gameBGM.mainMenu, amountOfTimeForFadeOut, 0, 1));
                currentlyPlayingBGM = gameBGM.mainMenu;
                break;
            case "ShipSelect":
                PlayShipSelectMusic();
                StartCoroutine(FadeBGM(gameBGM.shipSelectMenu, amountOfTimeForFadeOut, 0, 1));
                currentlyPlayingBGM = gameBGM.shipSelectMenu;
                break;
            case "Level1":
                PlayLevelMusic(1);
                StartCoroutine(FadeBGM(gameBGM.level1, amountOfTimeForFadeOut, 0, 1));
                currentlyPlayingBGM = gameBGM.level1;
                break;
        }
    }

    static IEnumerator FadeBGM(AudioSource audioSource, float duration, float startVolume,float targetVolume)
    {
        float currentTime = 0;
        float start = startVolume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);            
            yield return null;
        }
        yield break;
    }

    #endregion


    public void PlayerPickedUpPowerUp(PowerUpType type)
    {
        gameSFX.pickUp.Play();
        
        switch (type)
        {
            case PowerUpType.ShootLvlUp:
                gameSFX.playerShot.Play();
                break;
            case PowerUpType.Shield:

                break;
            case PowerUpType.SpecialAtk:

                break;
        }
    }
}

[System.Serializable]
public class GameSFX
{
    public AudioSource shipExplosion;
    public AudioSource playerShot;
    public AudioSource shotPowerUp;
    public AudioSource pickUp;
    public AudioSource barrelRoll;
    public AudioSource shieldBroken;
    public AudioSource mouseHoverOver;
    public AudioSource mouseClick;
}

[System.Serializable]
public class GameBGM
{
    public AudioSource mainMenu;
    public AudioSource shipSelectMenu;
    public AudioSource level1;
}

