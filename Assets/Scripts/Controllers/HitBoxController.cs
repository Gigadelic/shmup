﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxController : MonoBehaviour
{
    public ShipController shipController;
    public string tagToLookFor;
    public BoxCollider hitBoxCollider;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == tagToLookFor)
        {
            shipController.TakeDamage(other.gameObject.GetComponent<BulletController>().dmgAmount);
            Destroy(other.gameObject);
        }
    }

    public void DisableCollider()
    {
        hitBoxCollider.enabled = false;
    }

    public void EnableCollider()
    {
        hitBoxCollider.enabled = true;
    }
}
