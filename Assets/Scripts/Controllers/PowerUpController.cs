﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    public PowerUpType powerUpType;
    public float moveSpeed = 3f;

    private void Update()
    {
        transform.Translate(new Vector3(0, 0, -1 * moveSpeed * Time.deltaTime));
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            //Player has picked up the power up
            PowerUpPickedUp();            
        }
    }

    public void PowerUpPickedUp()
    {
        EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_HAS_PICKED_UP_POWER_UP, new GameEventArgs((int)powerUpType));
        //Player Pick Particle ?
        Destroy(gameObject);
    }
}

public enum PowerUpType
{
    ShootLvlUp,
    Shield,
    SpecialAtk,
    None
}
