﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTableController : MonoBehaviour
{
    public List<DropPercentage> listOfDrops;

    public PowerUpType PickItemFromTable()
    {
        PowerUpType droppedType = PowerUpType.None;
        List<PowerUpType> dropTable = new List<PowerUpType>();

        //Fill List
        foreach (DropPercentage drop in listOfDrops)
        {
            for (int i = 0; i < drop.percentage; i++)
            {
                dropTable.Add(drop.type);
            }
        }

        //Pick From List
        droppedType = dropTable[Random.Range(0, dropTable.Count)];
        return droppedType;
    }

    public GameObject GetDropByType(PowerUpType type)
    {
        return listOfDrops.FindLast(t => t.type == type).drop;
    }
}

[System.Serializable]
public class DropPercentage
{
    public PowerUpType type;    
    public float percentage;
    public GameObject drop;
}
