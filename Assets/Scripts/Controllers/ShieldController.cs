﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : MonoBehaviour
{

    public int currentHP = 3;
    public int MaxHP = 3;
    public GameObject ShieldExplosionParticle;
    public string bulletTagToCheckFor;

    private void OnTriggerEnter(Collider other)
    {
        //has a bullet entered the Shield?
        if(other.gameObject.tag == bulletTagToCheckFor)
        {
            ShieldGotDamaged();
            Destroy(other.gameObject);
        }
    }
    private void OnEnable()
    {
        currentHP = MaxHP;
    }

    void ShieldGotDamaged()
    {
        currentHP--;
        //Play dmg indicator.
        if(currentHP <= 0)
        {
            Instantiate(ShieldExplosionParticle, transform.position, Quaternion.identity);
            EventManager.Instance.DispatchGameEvent(GameEventEnum.PLAYER_SHIELD_IS_BROKEN, GameEventArgs.Empty);
        }
    }
}
