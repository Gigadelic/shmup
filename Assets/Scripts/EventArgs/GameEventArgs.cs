﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventArgs : EventArgs {

    public int numberValue;
    public PlayerController player;
    public float floatValue;
    public ShipController shipController;
    public EnemyWaveController enemyWaveController;

    public GameEventArgs(int number)
    {
        numberValue = number;
    }

    public GameEventArgs(PlayerController player)
    {
        this.player = player;
    }

    public GameEventArgs(ShipController shipController)
    {
        this.shipController = shipController;
    }

    public GameEventArgs(float number)
    {
        floatValue = number;
    }

    public GameEventArgs(EnemyWaveController enemyWaveController)
    {
        this.enemyWaveController = enemyWaveController;
    }
    
}
