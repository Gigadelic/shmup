﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventEnum  {
        
    public const string ENEMY_SHIP_DESTROYED = "EnemyShipDestroyed";
    public const string ENEMY_WAVE_DESTROYED = "EnemyWaveDestoryed";
    public const string PLAYER_HAS_SHOT = "PlayerHasShot";
    public const string PLAYER_HAS_DIED = "PlayerHasDied";
    public const string PLAYER_HAS_PICKED_UP_POWER_UP = "PlayerHasPickUpPowerUp";    
    public const string PLAYER_HAS_RESPAWNED = "PlayerHasRespawned";
    public const string PLAYER_SHIELD_IS_BROKEN = "PlayerShieldIsBroken";
    public const string PLAYER_IS_DOING_A_BARREL_ROLL = "PlayerIsDoingABarrelRoll";
    public const string PLAYER_IS_GAME_OVER = "PlayerIsGameOver";
    
    public const string BOSS_TOOK_DAMAGE = "BossTookDamage";
    public const string BOSS_HAS_APPEARED = "BossHasAppeared";
    public const string BOSS_HAS_BEEN_DEFEATED = "BossHasBeenDefeated";
    public const string BOSS_HAS_EXPLODED = "BossHasExploded";

    public const string LOAD_SHIP_SELECT_MENU = "LoadShipSelectMenu";
    public const string LOAD_MAIN_MENU = "LoadMainMenu";
    public const string LOAD_LEVEL = "LoadLevel";
    public const string LOAD_OPTIONS_MENU = "LoadOptionsMenu";
    public const string GAME_LOADED = "GameLoaded";

    public const string LEVEL_LOADED = "LevelLoaded";

    public const string PLAYER_HOVERED_OVER_OPTION = "PlayerHoveredOverOption";
    public const string PLAYER_SELECTED_OPTION = "PlayerSelectedOption";

    public const string MESSAGE_DISPLAY_COMPLETE = "MessageDisplayComplete";
}
