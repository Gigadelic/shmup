﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventLogic {

    public GameEventLogic()
    {
        EventManager.Instance.GameEvent += OnGameEventHandler;
    }

    ~GameEventLogic()
    {
        EventManager.Instance.GameEvent -= OnGameEventHandler;
    }

    private void OnGameEventHandler(string type, EventArgs args)
    {
        GameEventArgs gameArgs = args as GameEventArgs;
        switch(type)
        {
            case GameEventEnum.ENEMY_SHIP_DESTROYED:
                GUIManager.Instance.SoundController.PlayExplosionClip();
                GUIManager.Instance.enemyWaveManager.EnemyHasBeenDestoryed(gameArgs.shipController);
                break;
            case GameEventEnum.PLAYER_HAS_SHOT:
                GUIManager.Instance.SoundController.PlayPlayerShot();
                break;
            case GameEventEnum.PLAYER_HAS_DIED:
                GUIManager.Instance.SoundController.PlayExplosionClip();
                GUIManager.Instance.continueController.PlayerHasDied();
                break;
            case GameEventEnum.PLAYER_IS_GAME_OVER:
                GUIManager.Instance.healthBarController.MakeHealthBarDisappear();                
                GUIManager.Instance.messageController.DisplayMessage("Game Over", MessageType.GameOver, 1f);
                break;
            case GameEventEnum.PLAYER_HAS_PICKED_UP_POWER_UP:
                GUIManager.Instance.player.PlayerPickedUpPowerUp((PowerUpType)gameArgs.numberValue);
                GUIManager.Instance.SoundController.PlayerPickedUpPowerUp((PowerUpType)gameArgs.numberValue);
                break;
            case GameEventEnum.PLAYER_HAS_RESPAWNED:
                GUIManager.Instance.player = gameArgs.player; //Set Reference to player
                gameArgs.player.Respawn();
                break;            
            case GameEventEnum.PLAYER_SHIELD_IS_BROKEN:
                GUIManager.Instance.player.DisableShield();
                GUIManager.Instance.SoundController.PlayShieldBroken();
                break;
            case GameEventEnum.PLAYER_IS_DOING_A_BARREL_ROLL:
                GUIManager.Instance.SoundController.PlayerBarrelRoll();
                break;
            case GameEventEnum.BOSS_HAS_APPEARED:
                GUIManager.Instance.healthBarController.SetUpHealthBar(gameArgs.floatValue);
                GUIManager.Instance.healthBarController.MakeHealthBarAppear();
                break;
            case GameEventEnum.BOSS_TOOK_DAMAGE:
                GUIManager.Instance.healthBarController.UpdateHealthBar(gameArgs.floatValue);
                break;
            case GameEventEnum.BOSS_HAS_BEEN_DEFEATED:
                GUIManager.Instance.healthBarController.UpdateHealthBar(0); //Empty Meter
                break;
            case GameEventEnum.BOSS_HAS_EXPLODED:
                GUIManager.Instance.SoundController.PlayExplosionClip();
                GUIManager.Instance.healthBarController.MakeHealthBarDisappear();                
                GUIManager.Instance.messageController.DisplayMessage("Mission Complete", MessageType.MissionComplete, 2f);
                break;
            case GameEventEnum.ENEMY_WAVE_DESTROYED:
                GUIManager.Instance.enemyWaveManager.EnemyWaveHasBeenDestoryed(gameArgs.enemyWaveController);
                GUIManager.Instance.enemyWaveManager.SpawnNextEnemyWave();
                break;

            #region Ui Menu Events
            case GameEventEnum.GAME_LOADED:
                GUIManager.Instance.SoundController.PlayMainMenuMusic();
                break;
            case GameEventEnum.LOAD_MAIN_MENU:
                GUIManager.Instance.LoadMainMenu();                
                break;
            case GameEventEnum.LOAD_SHIP_SELECT_MENU:
                GUIManager.Instance.LoadShipSelect();
                GUIManager.Instance.messageController.DisplayMessage("Select Your Ship", 0, 3f);
                GUIManager.Instance.SoundController.PlayMouseClick();                
                break;
            case GameEventEnum.LOAD_OPTIONS_MENU:
                break;
            case GameEventEnum.LOAD_LEVEL:
                Debug.Log("Load Level: " + gameArgs.numberValue);
                GUIManager.Instance.LoadLevel(gameArgs.numberValue);
                //GUIManager.instance.SoundController.PlayLevelMusic(gameArgs.numberValue);
                break;
            case GameEventEnum.LEVEL_LOADED:
                if(!GUIManager.Instance.player)
                    GUIManager.Instance.player = gameArgs.player;
                GUIManager.Instance.player.Respawn();
                GUIManager.Instance.continueController.SetUpPlayerContinues();
                GUIManager.Instance.messageController.DisplayMessage("Mission Start", MessageType.MissionStart, 2f);
                break;
            case GameEventEnum.PLAYER_HOVERED_OVER_OPTION:
                GUIManager.Instance.SoundController.PlayMouseHover();
                break;
            case GameEventEnum.PLAYER_SELECTED_OPTION:
                GUIManager.Instance.SoundController.PlayMouseClick();
                break;
            case GameEventEnum.MESSAGE_DISPLAY_COMPLETE:
                switch ((MessageType)gameArgs.numberValue)
                {
                    case MessageType.MissionStart:
                        GUIManager.Instance.enemyWaveManager.SpawnNextEnemyWave();
                        break;
                    case MessageType.MissionComplete:
                        GUIManager.Instance.continueController.ResetContinues();
                        GUIManager.Instance.LoadMainMenu();
                        break;
                    case MessageType.GameOver:
                        GUIManager.Instance.continueController.ResetContinues();
                        GUIManager.Instance.LoadMainMenu();
                        break;
                }
                break;
            #endregion
            default:
                Debug.LogWarning("EVENT WAS FIRED BUT NEVER CAUGHT");
                break;
        }
    }
	
}
