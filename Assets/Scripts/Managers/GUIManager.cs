﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIManager : MonoBehaviour {

    public static GUIManager Instance { get; private set; }
    public SoundController SoundController;
    public ContinueController continueController;    
    public TransitionController transitionController;
    public HealthBarController healthBarController;
    public MessageController messageController;
    [Header("Level Specifics")]
    public PlayerController player;
    public EnemyWaveManager enemyWaveManager;       

    public GUIManager()
    {
        
    }

    ~GUIManager()
    {
        SceneManager.sceneLoaded -= OnSceneLoadedHandler;
    }


    void OnSceneLoadedHandler(Scene scene, LoadSceneMode mode)
    {
        if (Instance == this)
        {
            Debug.Log("Scene Loaded, Scene Name: " + scene.name);
            switch (scene.name)
            {
                case "MainMenu":
                    break;
                case "ShipSelect":
                    break;
                case "Level1":
                    LoadGameLevelSpecificReferences();
                    break;
            }
        }
    }

    public void LoadLevel(int levelNumber)
    {        
        StartCoroutine(LoadSceneWithTransition("Level" + levelNumber));
    }

    public void LoadMainMenu()
    {
        StartCoroutine(LoadSceneWithTransition("MainMenu"));
        //SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void LoadShipSelect()
    {
        StartCoroutine(LoadSceneWithTransition("ShipSelect"));        
    }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else if(Instance == null)
        {
            Instance = this;
            SceneManager.sceneLoaded += OnSceneLoadedHandler;
        }
        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);        
    }



  

    IEnumerator LoadSceneWithTransition(string sceneName)
    {
        transitionController.CloseDoorTransition();
        //FadeOutMusic
        SoundController.FadeOutBGM();
        yield return new WaitForSeconds(2f);
        //FadeInNewSong
        SoundController.FadeInBGM(sceneName);
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        transitionController.OpenDoorTransition();
    }

    void LoadGameLevelSpecificReferences()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
            EventManager.Instance.DispatchGameEvent(GameEventEnum.LEVEL_LOADED, new GameEventArgs(player));            
        }            

        if (enemyWaveManager == null)
        {
            enemyWaveManager = GameObject.FindGameObjectWithTag("EnemyWaveManager").GetComponent<EnemyWaveManager>();            
        }

        continueController.playerSpawnPoint = GameObject.FindGameObjectWithTag("PlayerSpawnPoint").transform;
    }

}
