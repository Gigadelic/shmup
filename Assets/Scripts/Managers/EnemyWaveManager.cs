﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaveManager : MonoBehaviour
{
    public List<EnemyWaveController> listOfEnemyWavesInOrder;
    public float spawnDelayTimeInSeconds = 1f;
    private Queue<EnemyWaveController> enemyWaveQueue = new Queue<EnemyWaveController>();
    private EnemyWaveController currentEnemyWave;

    // Start is called before the first frame update
    void Start()
    {
        foreach (EnemyWaveController wave in listOfEnemyWavesInOrder)
        {
            enemyWaveQueue.Enqueue(wave);
            wave.gameObject.SetActive(false);
        }
       
    }

    public void SpawnNextEnemyWave()
    {
        StartCoroutine(SpawnNextEnemyWaveWithDelay());
    }

    IEnumerator SpawnNextEnemyWaveWithDelay()
    {
        yield return new WaitForSeconds(spawnDelayTimeInSeconds);
        currentEnemyWave = enemyWaveQueue.Dequeue();
        currentEnemyWave.gameObject.SetActive(true);
    }

    public void EnemyWaveHasBeenDestoryed(EnemyWaveController wave)
    {
        if (listOfEnemyWavesInOrder.Contains(wave))
        {
            wave.gameObject.SetActive(false);
            listOfEnemyWavesInOrder.Remove(wave);
        }
    }

    public void EnemyHasBeenDestoryed(ShipController enemyShip)
    {
        currentEnemyWave.EnemyHasBeenDestoryed(enemyShip);
    }
}
