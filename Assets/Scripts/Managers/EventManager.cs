﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventManager  {

    //Events
    public delegate void GameEventHandler(string type, EventArgs args);
    public event GameEventHandler GameEvent;

    private static EventManager instance;

    //Singleton
    public static EventManager Instance
    {
        get
        {
            if (instance == null)
                instance = new EventManager();

            return instance;
        }
    }

    private EventManager()
    {

    }

    //Dispatch
    public void DispatchGameEvent(string type, EventArgs args)
    {
        if(GameEvent != null)
        {
            GameEvent(type, args);
        }
    }
}
