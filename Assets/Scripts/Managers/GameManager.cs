﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {


    //public static GameManager instance = null;
    private GameEventLogic _gameEventLogic;

    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            LoadLogicClasses();
        }
        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
        
    }
   
    // Use this for initialization
    void LoadLogicClasses () {

        //Init the Logic Classes.
        _gameEventLogic = new GameEventLogic();

    }
    
    
}
