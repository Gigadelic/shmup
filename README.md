Untitled Shoot em Up game.
====================

## General
Please note that this project is for personal development purposes only and is still a work in progress.

Developed and compiled with Unity 2019.4.3f1

## Getting Started
If you wish to start the game in the editor please load the MainMenu scene (Assets->Scenes) and hit run.

If prompted. Please import TMP.

You can also find a pre compiled build in Builds->FirstBuild.

## Controls
W,A,S,D or arrow keys to move the player.
Left mouse button to fire weapon.
C to do a barrel role.

## Power Ups
Weapon Upgrade: Upgrades the main guns to shoot in three directions.

Shield: Protects player from 3 bullets.

Lasers: Adds lasers on the side of the ship that increases damage.


## Notes
For demo puposes I gave the player a lot of health.

The boss can only take damage after his lazer cannon is exposed.

## Credits
All code, art and music (Minus minor UI and particles) has been created by Serge LeBlanc.